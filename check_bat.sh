#! /bin/bash
# set -xv
alertefaible=30
alertetresfaible=20
hibernate=5
# xdg-open
lastbranche=-1
lastcharge=-1
faible=0
tresfaible=0
luminositecharge=240000
luminositedecharge=200000
luminositefaible=2400
luminositetresfaible=240
user="/home/neo/"
TMP=${user}"TMP/bat_check.tmp"
luminositeenplacement="/sys/class/backlight/intel_backlight/brightness"
brancheenplacement="/sys/class/power_supply/AC/online"
chargeenplacement="/sys/class/power_supply/BAT0/capacity"
# shellcheck source=/home/neo/TMP/bat_check.tmp
source "${TMP}"
#pkexec $user"Programes/backlight.sh"
# cat $TMP | while read ligne
# do
#     echo $ligne
# done
save (){
	echo "save"
	echo "luminositecharge=${luminositecharge}" > "${TMP}"
	{
        echo "luminositedecharge=${luminositedecharge}"
	    echo "luminositefaible=${luminositefaible}"
	    echo "luminositetresfaible=${luminositetresfaible}"
    } >> "${TMP}"
}
brancher (){
    branche=$( cat "${brancheenplacement}")
    # echo $branche
    if [ "${branche}" -eq 1 ]
    then
        if [ "${lastbranche}" -ne "${branche}" ]
        then
            echo "L'ordinateur est branché."
            # notify-send Batterie "L'ordinateur branché.wav &
            cvlc "${user}Téléchargements/Sons/power-plug.oga" --play-and-exit &
            faible=0
            tresfaible=0
            luminosite "${luminositecharge}"
        fi
        lastluminositecharge="${luminositecharge}"
        luminositecharge=$(cat "${luminositeenplacement}")
        if [ "${luminositecharge}" -ne "${lastluminositecharge}" ]
        then
			save
        fi

    else
        if [ "${lastbranche}" -ne "${branche}" ]
        then
            echo "L'ordinateur est débranché."
            # notify-send Batterie "L'ordinateur est débranché." &
            cvlc "${user}Téléchargements/Sons/power-unplug.oga" --play-and-exit &
            luminosite "${luminositedecharge}"
        fi
        lastluminositedecharge="${luminositedecharge}"
        luminositedecharge=$(cat "${luminositeenplacement}")
        if [ "${luminositedecharge}" -ne "${lastluminositedecharge}" ]
        then
			save
        fi
        charge
    fi
        # echo $lastbranche
        # echo $branche
        lastbranche="${branche}"
}
charge (){
    charge=$( cat "${chargeenplacement}")
    if [ "${charge}" -le "${alertefaible}" ]
    then
        if [ "${charge}" -le "${hibernate}" ]
        then
            systemctl hibernate
        fi
        if [ "${charge}" -le "${alertetresfaible}" ]
        then
            if [ "${tresfaible}" -eq 0 ]
            then
                if [ "${lastcharge}" -ne "${charge}" ]
                then

                    tresfaible=1
                    faible=0
                    # notify-send Batterie "Batterie très faible."
                    echo "Batterie très faible."
                    zenity --title="Batterie tres faible" --window-icon=~/images/bat.png --warning --text="La battrie est très faible\, veuiller brancher rapidement l'ordinateur" --ellipsize &
                    aplay "${user}Téléchargements/Sons Windows/Windows Hardware Fail.wav" &
                    luminosite "${luminositetresfaible}"
                fi
            else
                lastluminositetresfaible="${luminositetresfaible}"
                luminositetresfaible=$(cat "${luminositeenplacement}")
                if [ "${luminositetresfaible}" -ne "${lastluminositetresfaible}" ]
                then
					save
                fi
            fi
        else
            if [ "${faible}" -eq 0 ]
            then
                if [ "${lastcharge}" -ne "${charge}" ]
                then
                    tresfaible=0
                    faible=1
                    # notify-send Batterie "Batterie faible"
                    echo "Batterie faible."
                    zenity --title="Batterie faible" --window-icon=~/images/bat.png --warning --text="La battrie est faible\, veuiller brancher l'ordinateur" --ellipsize &
                    aplay é"${user}Téléchargements/Sons Windows/Windows Notify Email.wav" &
                    luminosite "${luminositefaible}"
                fi
            else
                lastluminositefaible="${luminositefaible}"
                luminositefaible=$(cat "${luminositeenplacement}")
                if [ "${lastluminositefaible}" -ne "${luminositefaible}" ]
                then
					save
                fi
            fi
        fi
    else
        faible=0
        tresfaible=0
    fi
        lastcharge=${charge}
}
luminosite () {
    entrer=$1
    LUMINOSITE=${entrer}
    # LUMINOSITE=$(echo $entrer/10 | bc)
    # echo $LUMINOSITE
    echo "${LUMINOSITE}" > "${luminositeenplacement}"
    # entrer=$(zenity --scale --value=60 --min-value=20 --text="INTENSITE" --title="LUMINOSITE")
    # LUMINOSITE=$(echo $entrer/100 | bc -l)
    # LUMINOSITE=$(echo $entrer/100 | bc -l)
    # screen=$(xrandr | awk 'BEGIN{} $2=="connected" {print $1}END{}')
    # xrandr --output $screen --brightness $LUMINOSITE
}
while true
do
    sleep .5
    brancher
    # read text
    # echo $text
    # if [ $text = "stop" ]
    # then
    #     exit 0
    # fi
done
