#!/usr/bin/env python3
"""Script to run a python script with rich traceback."""
# This script takes a python script as input and runs it with rich traceback.
# It is useful for debugging python scripts.
#
# Example:
#   python py_rich_traceback.py my_script.py
#
# It is also possible to pass arguments to the script.
#
# Example:
#   python py_rich_traceback.py my_script.py arg1 arg2
#
# Import required modules
import sys

import rich
import rich.traceback
import rich.pretty

# Initialise rich traceback.
rich.traceback.install(show_locals=True, theme="gruvbox-dark")

# Get the script name and arguments if any. If no arguments are passed,
# raise an error.
if len(sys.argv) < 2:
    raise ValueError("No script name passed.")
script_name = sys.argv[1]
script_args = sys.argv[2:]

# Edit argv to remove the script name and arguments.
sys.argv = [script_name] + script_args

# Log the script name and arguments.
rich.print(f"Script name: {script_name}")
rich.print(f"Script arguments: {script_args}")
# for var in list(locals().keys()):
#     if var != "script_name":
#         print(f"Removing {var} from locals()")
#         del locals()[var]
# Run the script.
with open(script_name) as f:
    code = compile(f.read(), script_name, "exec")
    exec(code)
